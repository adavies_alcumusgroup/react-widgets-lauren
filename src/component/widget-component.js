import React, {useContext} from "react"
import classnames from "classnames"
import {DocumentContext, EditModeContext} from "./contexts"
import isEqual from "lodash/isEqual"
import {inPriorityOrder, processRenderFunctions, widgets} from "./widgets"
import SwallowClicks from "./swallow-clicks"
import {ButtonGroup, Col, Row} from "reactstrap"
import injectSheet from "react-jss"
import {SortableContainer, SortableElement} from "react-sortable-hoc"

let color = "rgba(138, 192, 250, 0.075)"


const styles = {
    outer: {
        height: "100%"
    },
    widgetRow: {
        minHeight: 31,
        height: "100%"
    },
    widgetContext: {
        position: "relative"
    },
    widgetFrame: {
        position: "absolute",
        left: -4,
        right: -4,
        top: -4,
        bottom: -4,
        border: "none",
        borderRadius: 4,
        "&.active": {
            background: color
        }
    }
}

const Wrapper = (props) => {
    return props.children
}

const Widget = injectSheet(styles)(function Widget({classes, children, design, document, key, className = "", endOfLineClass = "", type = "unknown", focusOn, isSelected, ...props}) {
    let types = Array.isArray(type) ? type : [type]
    const editMode = useContext(EditModeContext)
    const context = useContext(DocumentContext)
    design = design || context.design
    document = document || context.document
    const {focus, setFocus} = context
    isSelected = editMode && (isSelected || isEqual(focus, focusOn || design))
    let inline = []
    let endOfLine = []
    if (isSelected) {
        const lineContext = {...context, inline, ok: true, endOfLine, design, document}
        types.forEach(type => {
            widgets.emit(`inline.${type}`, lineContext)
        })
        inline = processRenderFunctions(lineContext.inline, lineContext)
        endOfLine = processRenderFunctions(lineContext.endOfLine, lineContext)
    }
    let content = []
    if (children) content.push(() => <Wrapper key="children">{children}</Wrapper>)
    let contentContext = {...context, content, ok: true, design, document, isSelected}
    types.forEach(type => {
        widgets.emit(`editor.${type}`, contentContext)
    })
    content = processRenderFunctions(contentContext.content, contentContext)

    return <div key={key} onFocus={(event) => {
        setFocus(focusOn || design)
        event.stopPropagation()
    }} onClick={(event) => {
        setFocus(focusOn || design)
        event.stopPropagation()
    }} className={classes.widgetContext + " " + className}>
        {editMode && <div key={"frame"} className={classnames(classes.widgetFrame, {active: isSelected})}>
            &nbsp;
        </div>}
        <DocumentContext.Provider key={"content"} value={{...context, design, document, type}}>
            <Row key={"row"} className={classes.widgetRow}>
                <Col key={"column"}>
                    {content.length > 0 && (
                        content.map((Item, index) => <Item {...context} design={design} document={document} key={index}
                                                           type={type} {...props}/>)
                    )}
                </Col>

                {endOfLine.length > 0 && (
                    <Col key={"buttons"} xs="auto" className={`d-flex align-items-center ${endOfLineClass}`}>
                        <ButtonGroup size="sm">
                            {endOfLine.sort(inPriorityOrder).map((Item, index) => <Item {...context} design={design}
                                                                                        document={document} key={index}
                                                                                        type={type} {...props}/>)}
                        </ButtonGroup>
                    </Col>
                )}
            </Row>
            <SwallowClicks style={{position: "relative", zIndex: 2}}>
                {inline.length > 0 && (
                    inline.sort(inPriorityOrder).map((Item, index) => <div key={index}><Item {...context}
                                                                                             design={design}
                                                                                             document={document}
                                                                                             type={type} {...props}/>
                    </div>)
                )}
            </SwallowClicks>
        </DocumentContext.Provider>
    </div>
})

export default Widget
export {Widget}

const SortableWidget = SortableElement(Widget)
const SortableWidgetContainer = SortableContainer(Widget)

export {SortableWidget, SortableWidgetContainer}
