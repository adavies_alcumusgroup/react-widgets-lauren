import React from "react"

class ErrorBoundary extends React.Component {
    constructor(props) {
        super(props)
        this.state = {hasError: false}
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return {hasError: true}
    }

    componentDidCatch(error, info) {
        // You can also log the error to an error reporting service
    }

    render() {
        if (this.state.hasError) {
            // You can render any custom fallback UI
            return <h5>ERROR</h5>
        }

        return this.props.children
    }
}

export default function SwallowClicks({children, style}) {
    return (
        <ErrorBoundary>
            <div style={style} onFocus={event=>event.stopPropagation()} onClick={(event) => event.stopPropagation()}>
                {children}
            </div>
        </ErrorBoundary>
    )
}
