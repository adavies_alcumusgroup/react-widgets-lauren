/* This is pretty much the whole logic for the dashboard*/


import React, {useState} from "react"
import {
    componentAsPrioritisedFunction,
    inPriorityOrder,
    SortableWidget,
    SortableWidgetContainer,
    widgets, arrayMoveInPlace
} from "../component"
import {ButtonDropdown, Col, DropdownItem, DropdownMenu, DropdownToggle, FormGroup, Input, Label, Row} from "reactstrap"
import {generate} from "shortid"
import {heading} from "./styles"
import injectSheet from "react-jss"
import classnames from "classnames"
import {FiPlus} from "react-icons/fi"
import {ColorPicker} from "./color-picker"
import Switch from "@trendmicro/react-toggle-switch"
import '@trendmicro/react-toggle-switch/dist/react-toggle-switch.css'

const styles = {
    heading,
    questionRow: {
        margin: {
            top: 5
        }
    },
    tools: {
        padding: 0,
        margin: 0,
        zIndex: 2,
        position: "absolute",
        right: 0,
        bottom: 19
    },
    dashRow: {
        marginTop: 16
    }
}


const DashboardItems = injectSheet(styles)(function DashboardItems({doUpdate, editMode, dashboardItems, changed, classes, focus: {selectedWidgets, selectedWidget}, fallbackCaption, ...props}) {
    const dashboardContext = {dashboardItems, ...props, changed}
    widgets.emit("filter.dashboardItems", dashboardContext)
    return <SortableWidgetContainer axis="xy"
                                    distance={4}
                                    isSelected={selectedWidgets === dashboardItems}
                                    focusOn={{selectedWidgets: dashboardItems}}
                                    design={dashboardItems}
                                    onSortEnd={({oldIndex, newIndex}) => {
                                        arrayMoveInPlace(dashboardItems, oldIndex, newIndex)
                                        doUpdate()
                                    }}
                                    type="dashboardItems">
        {
            dashboardContext.dashboardItems.length
                ? <Row className={classes.dashRow}> {dashboardContext.dashboardItems.map((item, index) => (
                    <SortableWidget key={item.id || index}
                                    index={index}
                                    disabled={!editMode}
                                    design={{item, list: dashboardItems}}
                                    isSelected={selectedWidget === item}
                                    focusOn={{
                                        selectedWidgets: dashboardItems,
                                        selectedWidget: item
                                    }}
                                    endOfLineClass={classes.tools}
                                    className={`col-${item.colSize || "md"}-${item.width || 3}`}
                                    type={[`dashboardItem.${item.type || 'default'}`, "list", "copyable"]}/>))}
                </Row>
                : (
                    <div>
                        {fallbackCaption !== undefined ? fallbackCaption : "No widgets"}
                    </div>)
        }
    </SortableWidgetContainer>
})

const Render = ({design, document, ...props}) => {
    return <Col>
        <DashboardItems dashboardItems={design.dashboardItems} {...props}/>
    </Col>
}

widgets.on("edit.dashboard", function ({design, document, layout}) {
    if (design.dashboardItems) {
        layout.content.push(Render)
    }
})


export {DashboardItems}


function createDefaultItem(props) {
    return Object.assign({
        id: generate(),
        title: "",
        description: ""
    }, props)
}

export {createDefaultItem}


const AddDashboardItem = injectSheet(styles)(({classes, doUpdate, design, types}) => {
    const [open, setOpen] = useState(false)
    return (
        <Row key={"itemtypes"} className={classnames(classes.questionRow, "no-gutters")}>
            <Col>&nbsp;</Col>
            <Col xs="auto">
                <ButtonDropdown direction="left" size="sm" isOpen={open} toggle={() => setOpen(!open)}>
                    <DropdownToggle color="success">
                        <FiPlus/> Widget
                    </DropdownToggle>
                    <DropdownMenu>
                        {types.sort(inPriorityOrder).map((type, index) => (
                            <DropdownItem onClick={() => {
                                design.push(type.create())
                                doUpdate()
                            }} key={index}>
                                {type.name}
                            </DropdownItem>
                        ))}
                    </DropdownMenu>
                </ButtonDropdown>
            </Col>
        </Row>
    )
})

widgets.on("inline.dashboardItems", function ({design, document, inline, ok}) {
    let types = []
    widgets.emit("dashboardItems.types", types)
    ok && inline.push(props => <AddDashboardItem types={types} {...props}/>)
})

const DashboardInfo = injectSheet(styles)(function DashboardInfo({focus: {selectedWidget}, doUpdate, classes}) {
    return (
        <>
            <h4 className={classes.heading}>Dashboard Widget</h4>
            <FormGroup row >
                <Label sm={2}>Title</Label>
                <Col sm={10}>
                    <Input type="text" value={selectedWidget.title} onFocus={e => e.target.select()}
                           onChange={event => {
                               selectedWidget.title = event.target.value
                               doUpdate()
                           }}/>
                </Col>
            </FormGroup>
            <FormGroup row >
                <Label sm={2}>Description</Label>
                <Col sm={10}>
                    <Input type="text" value={selectedWidget.description} onFocus={e => e.target.select()}
                           onChange={(event) => {
                               selectedWidget.description = event.target.value
                               doUpdate()
                           }}/>
                </Col>
            </FormGroup>
            <h4 className={classes.heading}>Chart</h4>
            <FormGroup row >
                <Label sm={2}>Background</Label>
                <Col sm={10}>
                    <ColorPicker value={selectedWidget.color || "#ffffff"} onChange={color => {
                        selectedWidget.color = color.hex
                        doUpdate()
                    }}/>
                </Col>
            </FormGroup>
            <FormGroup row >
                <Label sm={2}>Foreground</Label>
                <Col sm={10}>
                    <ColorPicker value={selectedWidget.axisColor || "#222"} onChange={color => {
                        selectedWidget.axisColor = color.hex
                        doUpdate()
                    }}/>
                </Col>
            </FormGroup>
            <FormGroup row >
                <Label sm={2}>Adjust</Label>
                <Col sm={10}>
                    <ColorPicker value={selectedWidget.adjustColor || "#000"} onChange={color => {
                        selectedWidget.adjustColor = color.hex
                        doUpdate()
                    }}/>
                </Col>
            </FormGroup>
        </>
    )
})

const StretchInfo = injectSheet(styles)(function StretchInfo({design, doUpdate, classes}) {
    return <>
        <h4 key={1} className={classes.heading}>Cards</h4>
        <FormGroup key={2} row>
            <Label sm={2}>Expand Cards</Label>
            <Col sm={10}>
                <Switch type="checkbox" checked={design.fillHeight}
                        onChange={() => {
                            design.fillHeight = !design.fillHeight
                            doUpdate()
                        }}/>
            </Col>
        </FormGroup>
    </>
})

widgets.on("edit.dashboard", function ({editor, focus: {selectedWidget}}) {
    selectedWidget && editor.tabs.general.content.push(componentAsPrioritisedFunction(DashboardInfo, 2))
    editor.tabs.general.content.push(componentAsPrioritisedFunction(StretchInfo, 10))
})
