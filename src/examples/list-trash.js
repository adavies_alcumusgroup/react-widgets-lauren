import {widgets, componentAsPrioritisedFunction} from "../component/index"
import {Button} from "reactstrap"
import {FiTrash} from "react-icons/fi"
import React from "react"

const Render = ({doUpdate, design}) => (
    <Button key={"trash"} onClick={() => {
        let {list, item} = design
        list.splice(list.findIndex(i => i === item), 1)
        doUpdate()
    }} color="danger">
        <FiTrash/>
    </Button>
)


widgets.on("inline.list", function ({endOfLine, ok}) {
    ok && endOfLine.push(componentAsPrioritisedFunction(Render, 100))
})
