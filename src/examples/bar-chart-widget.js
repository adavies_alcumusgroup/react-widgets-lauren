import React from "react"
import {componentAsPrioritisedFunction, widgets} from "../component/index"
import {createDefaultItem} from "./dashboard-items"
import {Card, CardBody, CardText, CardTitle} from "reactstrap"
import {Bar, BarChart, Cell, ResponsiveContainer, XAxis, YAxis} from "recharts"
import injectSheet from "react-jss"
import color from "./colors"
import {DUMMY_DATA} from "./example/dummy"


widgets.on("dashboardItems.types", types => {
    types.push({
        name: "Bar Chart",
        create: () => createDefaultItem({type: "bar", data: [], color: "#fcfcfc"}),
    })
})

const style = {
    card: {
        marginBottom: 32,
        overflow: "hidden",
    },
    chart: {
        background: "white",
    },
}

const Render = injectSheet(style)(({value, setValue, root, design: {item}, classes}) => {
    let data = item.data.length ? item.data : DUMMY_DATA
    const {height = 3} = item

    return (

        <Card className={classes.card} style={{height: root.fillHeight ? "calc(100% - 32px)" : undefined}}>
            <ResponsiveContainer height={height * 75}>
                <BarChart style={{background: item.color || "#fff"}} className={classes.chart}
                          margin={{top: 20, right: 20, left: 0, bottom: 0}} data={data}>
                    <XAxis dataKey={item.series || "name"} stroke={item.axisColor || "#333"}/>
                    <YAxis stroke={item.axisColor || "#333"}/>
                    <Bar dataKey={item.value || "value"}>
                        {data.map((_, i) => {
                            return <Cell key={i} fill={color(i, undefined, item.adjustColor)}/>
                        })}
                    </Bar>
                </BarChart>
            </ResponsiveContainer>
            {(item.title || item.description) && <CardBody>
                {!!item.title && <CardTitle dangerouslySetInnerHTML={{__html: item.title}}/>}
                {!!item.description && <CardText dangerouslySetInnerHTML={{__html: item.description}}/>}
            </CardBody>}
        </Card>

    )
})


widgets.on("editor.dashboardItem.bar", ({content, ok}) => {
    ok && content.push(componentAsPrioritisedFunction(Render))
})
