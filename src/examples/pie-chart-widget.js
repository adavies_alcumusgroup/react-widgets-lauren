import React from "react"
import {componentAsPrioritisedFunction, widgets} from "../component/index"
import {createDefaultItem} from "./dashboard-items"
import {Card, CardBody, CardText, CardTitle} from "reactstrap"
import {Cell, Legend, Pie, PieChart, ResponsiveContainer} from "recharts"
import injectSheet from "react-jss"
import color from "./colors"
import {DUMMY_DATA} from "./example/dummy"


widgets.on("dashboardItems.types", types => {
    types.push({
        name: "Pie Chart",
        create: () => createDefaultItem({type: "pie", data: [], color: "#fcfcfc"}),
    })
})

const style = {
    card: {
        marginBottom: 32,
        overflow: "hidden",
    },
    chart: {},
}

const Render = injectSheet(style)(({root, value, setValue, design: {item}, classes}) => {
    let data = item.data.length ? item.data : DUMMY_DATA
    const {height = 3} = item

    return (

        <Card className={classes.card} style={{height: root.fillHeight ? "calc(100% - 32px)" : undefined}}>
            <ResponsiveContainer height={height * 75}>
                <PieChart style={{background: item.color || "#fff"}} className={classes.chart}
                          margin={{top: 20, right: 10, left: 0, bottom: 0}}>
                    <Pie data={data} dataKey={item.value || "value"} nameKey={item.name || "name"} label>
                        {data.map((_, i) => {
                            return <Cell key={i} fill={color(i, undefined, item.adjustColor)}/>
                        })}
                    </Pie>
                    <Legend/>
                </PieChart>
            </ResponsiveContainer>
            {(item.title || item.description) && <CardBody>
                {!!item.title && <CardTitle dangerouslySetInnerHTML={{__html: item.title}}/>}
                {!!item.description && <CardText dangerouslySetInnerHTML={{__html: item.description}}/>}
            </CardBody>}
        </Card>

    )
})


widgets.on("editor.dashboardItem.pie", ({content, ok}) => {
    ok && content.push(componentAsPrioritisedFunction(Render))
})
