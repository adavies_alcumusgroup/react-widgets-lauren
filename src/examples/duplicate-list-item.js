import {componentAsPrioritisedFunction, widgets} from "../component/index"
import {generate} from "shortid"
import {Button} from "reactstrap"
import {FiCopy} from "react-icons/fi"
import React from "react"

const Render = ({doUpdate, design}) => (
    <Button key={"duplicate"} onClick={() => {
        let {list, item} = design
        let copy = JSON.parse(JSON.stringify(item))
        copy.id = generate()
        list.splice(list.findIndex(i => i === item) + 1, 0, copy)
        doUpdate()
    }} color="success">
        <FiCopy/>
    </Button>
)


widgets.on("inline.copyable", function ({endOfLine, ok}) {
    ok && endOfLine.push(componentAsPrioritisedFunction(Render, -1))
})
