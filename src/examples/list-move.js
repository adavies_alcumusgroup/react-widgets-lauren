import {widgets} from "../component/index"
import {Button} from "reactstrap"
import {FiChevronUp, FiChevronDown} from "react-icons/fi"
import {componentAsPrioritisedFunction} from "../component/index"
import React from "react"

const ListMove = ({doUpdate, design}) => {
    let {list, item} = design
    let index = list.indexOf(item)
    return (
        <>
            <Button disabled={index === 0} onClick={() => {
                list.splice(index, 1)
                list.splice(index-1, 0, item)
                doUpdate()
            }} color="primary">
                <FiChevronUp/>
            </Button>
            <Button disabled={index === list.length-1} onClick={() => {
                list.splice(index, 1)
                list.splice(index + 1, 0, item)
                doUpdate()
            }} color="primary">
                <FiChevronDown/>
            </Button>
        </>
    )
}

widgets.on("inline.list", function ({endOfLine, ok}) {
    ok && endOfLine.push(componentAsPrioritisedFunction(ListMove, 50))
})
