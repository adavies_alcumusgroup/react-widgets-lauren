# Widgets

FORKING Brilliant.

The widgets library is a series of components that provide a canvas for designing and then editing or viewing documents. The documents are expressed as Javascript objects, serializable to JSON.

The principle is to have a WYSIWYG editor for your own documents, to enable highly customised systems. You might use it to create a dynamic form filling interface where you can allow your users to design a series of forms or perhaps a dashboard designer and catalogue.

The library contains the bare minimum of components and uses a loosely coupled approach to editors, meaning that you can write code to use the canvas the way you see fit, and you can augment the experience later without changing any base code, plus you can reuse useful editors across many types of document.

![Image](https://s3-eu-west-1.amazonaws.com/shared-localstorage/demo.gif)

In the example you see above, most of the look and funcitonality has been developed in the examples, it's all very simple. The _Widgets_ component is just providing some places to put content and triggering events so you can loosely accrete functionality as you go along.

## Installation

`npm install --save react-ioc-widgets`

## Usage

1. First mount a Widgets component somewhere, make it `editable`
2. Provide it with a design document and a content document (JS objects, can be blank!)
3. Give it a type or add a `type` property to the design document

At this point your document will be displayed, but there isn't much to do with it. You need to loosely couple editors to the document using the `widgets` events from the library.

```jsx
import React from "react";
import { Widgets, widgets } from "react-widgets";

let document = {};
let design = { type: "test" };

function MyEditor() {
	return (
		<Widgets
			design={design}
			document={document}
			editable
			updated={() => console.log("Edited")}
		/>
	);
}

//Mount this somewhere
export default MyEditor;
```

Now you can create components which render the content of the document and provide editing

```jsx
import React, {useState} from "react"
import {Label, Input, FormGroup} from "reactstrap"
import {widgets, componentAsPrioritisedFunction} from "react-widgets"

function DisplayComponent({design}) {
    return <div>{design.title || "No Title"}</div>
}

function EditComponent({design, queueUpdate}) {
    const [title, setTitle] = useState(design.title || "")
    return <FormGroup>
            <Label>Title</Label>
            <Input type="text" value={title} onChange={e=>{
                setTitle(e.target.value)
                design.title = e.target.value
                queueUpdate()
            }}/>
        </FormGroup>
}

widgets.on("edit.test", function({design, editor: {tabs}, layout) {
    tabs.general.content.push(componentAsPrioritisedFunction(EditComponent, 2))
    layout.content.push(componentAsPrioritisedFunction(DisplayComponent))
})

```

You can also use the Widget, SortableWidget and SortableWidgetContainer components to create parts of your document that can be individually selected and edited, or moved around etc. See the examples for more details.

### Project Structure

The project examples are all in the `src/` folder. The `index.js` and `App.js` files make an example React app that shows off all of the features and the code for the editors etc can be found in the `src/examples/` folder.

`src/component` contains the code for the actual component, but it is exposed through the `index.js` in the project root.

You can find some example JSON that you could write to localStorage to get the examples bootstrapped in the `src/examples/example` folder.

### Examples

Install the component in development mode and then `npm start` to start the example app on localhost:3009
