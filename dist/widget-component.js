"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SortableWidgetContainer = exports.SortableWidget = exports.Widget = exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _classnames = _interopRequireDefault(require("classnames"));

var _contexts = require("./contexts");

var _isEqual = _interopRequireDefault(require("lodash/isEqual"));

var _widgets = require("./widgets");

var _swallowClicks = _interopRequireDefault(require("./swallow-clicks"));

var _reactstrap = require("reactstrap");

var _reactJss = _interopRequireDefault(require("react-jss"));

var _reactSortableHoc = require("react-sortable-hoc");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var color = "rgba(138, 192, 250, 0.075)";
var styles = {
  outer: {
    height: "100%"
  },
  widgetRow: {
    minHeight: 31,
    height: "100%"
  },
  widgetContext: {
    position: "relative"
  },
  widgetFrame: {
    position: "absolute",
    left: -4,
    right: -4,
    top: -4,
    bottom: -4,
    border: "none",
    borderRadius: 4,
    "&.active": {
      background: color
    }
  }
};

var Wrapper = function Wrapper(props) {
  return props.children;
};

var Widget = (0, _reactJss.default)(styles)(function Widget(_ref) {
  var classes = _ref.classes,
      children = _ref.children,
      design = _ref.design,
      document = _ref.document,
      key = _ref.key,
      _ref$className = _ref.className,
      className = _ref$className === void 0 ? "" : _ref$className,
      _ref$endOfLineClass = _ref.endOfLineClass,
      endOfLineClass = _ref$endOfLineClass === void 0 ? "" : _ref$endOfLineClass,
      _ref$type = _ref.type,
      type = _ref$type === void 0 ? "unknown" : _ref$type,
      focusOn = _ref.focusOn,
      isSelected = _ref.isSelected,
      props = _objectWithoutProperties(_ref, ["classes", "children", "design", "document", "key", "className", "endOfLineClass", "type", "focusOn", "isSelected"]);

  var types = Array.isArray(type) ? type : [type];
  var editMode = (0, _react.useContext)(_contexts.EditModeContext);
  var context = (0, _react.useContext)(_contexts.DocumentContext);
  design = design || context.design;
  document = document || context.document;
  var focus = context.focus,
      setFocus = context.setFocus;
  isSelected = editMode && (isSelected || (0, _isEqual.default)(focus, focusOn || design));
  var inline = [];
  var endOfLine = [];

  if (isSelected) {
    var lineContext = _objectSpread({}, context, {
      inline: inline,
      ok: true,
      endOfLine: endOfLine,
      design: design,
      document: document
    });

    types.forEach(function (type) {
      _widgets.widgets.emit("inline.".concat(type), lineContext);
    });
    inline = (0, _widgets.processRenderFunctions)(lineContext.inline, lineContext);
    endOfLine = (0, _widgets.processRenderFunctions)(lineContext.endOfLine, lineContext);
  }

  var content = [];
  if (children) content.push(function () {
    return _react.default.createElement(Wrapper, {
      key: "children"
    }, children);
  });

  var contentContext = _objectSpread({}, context, {
    content: content,
    ok: true,
    design: design,
    document: document,
    isSelected: isSelected
  });

  types.forEach(function (type) {
    _widgets.widgets.emit("editor.".concat(type), contentContext);
  });
  content = (0, _widgets.processRenderFunctions)(contentContext.content, contentContext);
  return _react.default.createElement("div", {
    key: key,
    onFocus: function onFocus(event) {
      setFocus(focusOn || design);
      event.stopPropagation();
    },
    onClick: function onClick(event) {
      setFocus(focusOn || design);
      event.stopPropagation();
    },
    className: classes.widgetContext + " " + className
  }, editMode && _react.default.createElement("div", {
    key: "frame",
    className: (0, _classnames.default)(classes.widgetFrame, {
      active: isSelected
    })
  }, "\xA0"), _react.default.createElement(_contexts.DocumentContext.Provider, {
    key: "content",
    value: _objectSpread({}, context, {
      design: design,
      document: document,
      type: type
    })
  }, _react.default.createElement(_reactstrap.Row, {
    key: "row",
    className: classes.widgetRow
  }, _react.default.createElement(_reactstrap.Col, {
    key: "column"
  }, content.length > 0 && content.map(function (Item, index) {
    return _react.default.createElement(Item, _extends({}, context, {
      design: design,
      document: document,
      key: index,
      type: type
    }, props));
  })), endOfLine.length > 0 && _react.default.createElement(_reactstrap.Col, {
    key: "buttons",
    xs: "auto",
    className: "d-flex align-items-center ".concat(endOfLineClass)
  }, _react.default.createElement(_reactstrap.ButtonGroup, {
    size: "sm"
  }, endOfLine.sort(_widgets.inPriorityOrder).map(function (Item, index) {
    return _react.default.createElement(Item, _extends({}, context, {
      design: design,
      document: document,
      key: index,
      type: type
    }, props));
  })))), _react.default.createElement(_swallowClicks.default, {
    style: {
      position: "relative",
      zIndex: 2
    }
  }, inline.length > 0 && inline.sort(_widgets.inPriorityOrder).map(function (Item, index) {
    return _react.default.createElement("div", {
      key: index
    }, _react.default.createElement(Item, _extends({}, context, {
      design: design,
      document: document,
      type: type
    }, props)));
  }))));
});
exports.Widget = Widget;
var _default = Widget;
exports.default = _default;
var SortableWidget = (0, _reactSortableHoc.SortableElement)(Widget);
exports.SortableWidget = SortableWidget;
var SortableWidgetContainer = (0, _reactSortableHoc.SortableContainer)(Widget);
exports.SortableWidgetContainer = SortableWidgetContainer;